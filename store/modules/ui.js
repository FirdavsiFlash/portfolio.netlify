import Vue from 'vue'
import Vuex from 'vuex'
import index from '../index'

Vue.use(Vuex)

const state = () => ({
  modals: {
    contacts: {
      status: false,
      courseList: false
    },
    mobile_menu: {
      status: false,
    },
    skilsAndPrograms: [{
        title: 'javascript',
        src: 'https://cdn-icons.flaticon.com/png/512/721/premium/721671.png?token=exp=1654097373~hmac=b420cfdee01c1883eff278e92b9e7910'
      },
      {
        title: 'vue',
        src: 'https://cdn1.iconfinder.com/data/icons/ionicons-fill-vol-2/512/logo-vue-512.png'
      },
      {
        title: 'nuxt',
        src: 'https://cdn4.iconfinder.com/data/icons/logos-brands-5/24/nuxt-dot-js-512.png'
      },
      {
        title: 'gitlab',
        src: 'https://cdn3.iconfinder.com/data/icons/font-awesome-brands/512/gitlab-512.png'
      },
      {
        title: 'tailwind',
        src: 'https://cdn2.iconfinder.com/data/icons/boxicons-logos/24/bxl-tailwind-css-512.png'
      }, {
        title: 'mongodb',
        src: 'https://cdn4.iconfinder.com/data/icons/logos-brands-5/24/mongodb-512.png'
      }, {
        title: 'postman',
        src: 'https://cdn4.iconfinder.com/data/icons/logos-brands-5/24/postman-512.png'
      }, {
        title: 'scss',
        src: 'https://cdn1.iconfinder.com/data/icons/bootstrap-vol-3/16/filetype-scss-512.png'
      }, {
        title: 'sass',
        src: 'https://cdn4.iconfinder.com/data/icons/logos-brands-5/24/sass-512.png'
      },
      {
        title: 'vscode',
        src: 'https://cdn1.iconfinder.com/data/icons/akar-vol-2/24/vscode-fill-512.png'
      }, {
        title: 'figma',
        src: 'https://cdn2.iconfinder.com/data/icons/css-vol-2/24/figma-512.png'
      },
      {
        title: 'adobe xd',
        src: 'https://cdn4.iconfinder.com/data/icons/logos-and-brands-1/512/3_Xd_Adobe_logo_logos-512.png'
      },
    ]
  },
})

const getters = {
  modals: state => state.modals,
}

const mutations = {
  CLOSE_ALL_MODALS(state) {
    for (let item in state.modals) {
      state.modals[item].status = false
    }
  },
  MANAGE_MODAL(state, details) {
    console.log("click");
    state.modals[details.name].status = !state.modals[details.name].status
    state.modals[details.name].text = details.text
  },
}

const actions = {

}

export default {
  state,
  getters,
  actions,
  mutations
}
