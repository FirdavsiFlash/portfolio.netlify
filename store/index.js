import Vuex from 'vuex'
import Vue from 'vue'
import ui from './modules/ui.js'
import vueSmoothScroll from 'vue-smooth-scroll'
Vue.use(vueSmoothScroll)

Vue.use(Vuex)

const state = () => ({

})

const getters = {

}

const mutations = {

}

const actions = {

}

const modules = {
  ui,
}

export default {
  state,
  getters,
  actions,
  mutations,
  modules
}
